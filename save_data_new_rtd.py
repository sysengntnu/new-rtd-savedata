import os

class SaveDataToFile:

	def __init__(self, path_to_save_file, name_of_save_file):
		''' Create a save object '''
		self.path_to_save_file = path_to_save_file
		self.name_of_save_file = name_of_save_file
		pass

	def save_data(self, data):
		''' When called it will write the data (dict) to the file'''
		self.data = data
		os.chdir(self.path_to_save_file)
		openfile = open(self.name_of_save_file,'a')
		data_name = data.keys()
		openfile.write('New data : \n')
		print data

		tmp_list = []
		for n in data_name:
			tmp =[]
			tmp.append((data[n]))
			tmp = tmp[0] #Simple way to remove parenthesis
			tmp_list.append(tmp)
			
		tmp_list = zip(*tmp_list)
		
		
		for x in tmp_list:
			x = ', '.join(map(str, x)) #Separe data by commas
			openfile.write(str(x))
			openfile.write('\n')
		openfile.close()
		pass


'''
This is a test for SaveDataToFile
'''

import os    
dat = {}
dat['voltage_1'] = 1, 0.1, 0.2, 0.3
dat['voltage_2'] = 2, 0.3, 0.1, 0.3
dat['voltage_3'] = 3, 25, 63, 1, 24, 88
dat['voltage_4'] = 4, 1, 9
dat['voltage_5'] = 5, 9, 7, 85, 41, 29, 18, 27
saveFile = SaveDataToFile('C:\shared_files', 'test_1.txt')
saveFile.save_data(dat)
