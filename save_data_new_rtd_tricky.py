import os

class SaveDataToFile:

	def __init__(self, path_to_save_file, name_of_save_file):
		''' Create a save object '''
		self.path_to_save_file = path_to_save_file
		self.name_of_save_file = name_of_save_file
		pass

	def save_data(self, data):
		''' When called it will write the data (dict) to the file'''
		self.data = data
		os.chdir(self.path_to_save_file)
		openfile = open(self.name_of_save_file,'a')
		data_name = data.keys()
		openfile.write('New data : \n')
		# print data

		# turn data dict into a list of list l_o_l
		l_o_l=[]
		for n in data_name:
			tmp=[]
			tmp.append((data[n]))
			tmp = tmp[0] #Simple way to remove parenthesis
			l_o_l.append(tmp)

		#print l_o_l

		u = max(map(len,l_o_l))
		for i in range(0, u):
			v = len(l_o_l)
			tmp_2=[]
			for j in range (0, v):	
				tmp_2.append(l_o_l[j][i])
			
			# write data separated by commas without bracket
			for x in tmp_2: 
				print x
				openfile.write((str(x)))
				openfile.write(',')
			openfile.write('\n')
		
		openfile.close()
		pass


'''
This is a test for SaveDataToFile
'''

import os    
dat = {}
dat['voltage_1'] = 1, 0.1, 0.2, 0.3, 8, 4, 2
dat['voltage_2'] = 2, 0.3, 0.1, 0.3, 78, 9, 7
dat['voltage_3'] = 3, 25, 63, 1, 0.3, 44, 51
dat['voltage_4'] = 4, 1, 9, 6, 1.2, 47, 96
dat['voltage_5'] = 5, 9, 7, 85, 4, 74, 85
saveFile = SaveDataToFile('C:\shared_files', 'test_1.txt')
saveFile.save_data(dat)
